import com.esotericsoftware.kryonet.Client;

import java.io.IOException;

public class ScrapperClient {
    public class SomeResponse {
        public String text;
    }

    public class SomeRequest {
        public String text;
    }

    public void initClient() throws IOException {
        Client client = new Client();
        client.start();
        client.connect(5000, "127.0.0.1", 54555, 54777);

        SomeRequest someRequest = new SomeRequest();
        someRequest.text = "Het!";
        client.sendTCP(someRequest);
    }
}
