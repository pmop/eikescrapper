package query;

import entities.OAdvertItem;
import entities.OAdvertItemBuilder;
import entities.dao.OAdvertItemDAO;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Attributes;
import org.jsoup.select.Elements;
import utils.DOCDefs;
import utils.GlobalDefinitions;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

@Log(topic = "DefaultQuery")
class DefaultQuery {
    OAdvertItemBuilder oAdvertItemBuilder = new OAdvertItemBuilder(GlobalDefinitions.getOAdvertItemPool());
    String myQuery;

    public DefaultQuery(String query) {
        myQuery = query;
    }

    public class Process implements Iterator<OAdvertItem> {
        Elements toProcess;
        int i = 0;

        private OAdvertItem getoAdvertItem(Element element) throws Exception {
            Attributes attributes = element.attributes();
            String sid = attributes.get(DOCDefs.ATTR_OLX_ID);
            long id = Integer.parseInt(sid);
            String adLink = attributes.get(DOCDefs.ATTR_OLX_ADLINK),
                    adTitle = attributes.get(DOCDefs.ATTR_OLX_ADTITLE),
                    adImg = "",
                    adCategory = Optional.of(element.select(DOCDefs.S_OLX_AD_CATEGORY).text()).get();
            Element imgElem = element.selectFirst(DOCDefs.S_OLX_AD_IMG);
            if (imgElem != null) adImg = imgElem.attributes().get(DOCDefs.ATTR_OLX_AD_IMG);
            if (adImg.equals(DOCDefs.S_OLX_AD_DEFAULTIMG));
            Elements eladWhere = element.select(DOCDefs.S_OLX_ADREGION);
            String adWhere = eladWhere.get(0).text();

            String adNeigh = "";
            if (adWhere.lastIndexOf(",") > 0) {
                String[] locations = eladWhere.text().split(",");
                adNeigh = locations[1].replace(" ", "");
                adWhere = locations[0].replace(" ", "");

            }

            Elements dateElements = element.getElementsByClass(DOCDefs.S_OLX_ADDATE_CNAME);
            String[] sdates = null;
            if (dateElements.size() == 1) {
                // Date is in string Today HH:MM
                sdates = dateElements.get(0).text().split("\\s");
            } else if (dateElements.size() == 2) {
                // Date is  Today  ---- HH:MM
                sdates = new String[]{
                        dateElements.get(0).text(),
                        dateElements.get(1).text()
                };
            }

            String formatedDate = DOCDefs.getOlxDateString(sdates);
            Element elprice = element.selectFirst(DOCDefs.S_OLX_ADPRICE);
            String price;
            if (elprice == null) price = "-1.0";
            else price = elprice.text().replaceAll("[^\\d]", "");
            oAdvertItemBuilder.setId(id);
            oAdvertItemBuilder.setLink(adLink);
            oAdvertItemBuilder.setTitle(adTitle);
            oAdvertItemBuilder.setState("al");
            oAdvertItemBuilder.setCity(adWhere);
            oAdvertItemBuilder.setNeighbour(adNeigh);
            oAdvertItemBuilder.setPublishedDate(formatedDate);
            oAdvertItemBuilder.setImgLink(adImg);
            oAdvertItemBuilder.setCategory(adCategory);
            oAdvertItemBuilder.setPrice(Float.parseFloat(price));
            return oAdvertItemBuilder.getOdvertItem();
        }

        public Process(Elements toProcess) {
            this.toProcess = toProcess;
        }

        @Override
        public boolean hasNext() {
            return (i+i) < toProcess.size();
        }

        @Override
        public OAdvertItem next() throws NoSuchElementException {
            OAdvertItem ret = null;
            if (!hasNext())
                throw new NoSuchElementException();
            try {
                ret = getoAdvertItem(toProcess.get(i));
            } catch (Exception e) {
                log.warning(e.getMessage());
            }
            i = i+1;
            return ret;
        }
    }

    public void doQuery() {
        OAdvertItem[] results = null;
        int howMany = 0;
        try {
            Document doc = Jsoup.connect(myQuery).get();
            Elements advertElements = doc.select(DOCDefs.S_OLX_AD_ITEM);
            results = new OAdvertItem[advertElements.size()];
            for (Process it = new Process(advertElements); it.hasNext(); ) {
                OAdvertItem item = it.next();
                if (item != null) {
                    if (OAdvertItemDAO.insert(item)) {
                        log.info(" found:\n" + item);
                        howMany++;
                    }
                }
            }
        } catch (IOException e) {
            DefaultQuery.log.warning("Error: could not resolve query: " +
                    myQuery + " Reason: " + e.getMessage());
        }
        if(howMany > 0) log.info(" Found: " + howMany);
    }
}
