package query;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.DOCDefs;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

public abstract class QueryResultGenerator<ResultType> implements Iterator<ResultType> {
        Elements toProcess;
        Document rootDocument;
        boolean initializated = false;
        int i;
        public QueryResultGenerator(String myQuery) throws IOException {
            this.toProcess = toProcess;
            i = 0;
            rootDocument = Jsoup.connect(myQuery).get();
            toProcess = rootDocument.select(DOCDefs.S_OLX_AD_ITEM);
            initializated = true;
        }

        protected abstract ResultType processElement(Element element) throws Exception;

        @Override
        public boolean hasNext() {
            return initializated && ((i+i) < toProcess.size());
        }

        @Override
        public ResultType next() throws NoSuchElementException {
            ResultType ret = null;
            if (!hasNext())
                throw new NoSuchElementException();
            try {
                ret = processElement(toProcess.get(i));
            } catch (Exception ignored) {
            }
            i = i+1;
            return ret;
        }
}
