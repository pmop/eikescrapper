package query;

public interface Query<ResultType> {
    ResultType[] doQuery();
}
