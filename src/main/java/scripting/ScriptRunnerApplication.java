package scripting;


import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ScriptRunnerApplication {
    private final static ScriptManager scriptManager;
    private final static ScriptEngine scriptEngine; // grab an instance so we may run standalone scripts
    private static Path currentPath;

    static {
        scriptEngine = new ScriptEngineManager().getEngineByName("jruby");
        scriptManager = new ScriptManager(scriptEngine);
        scriptManager.loadFromFolder("C:\\Users\\pedro\\Documents\\Scripts");
        currentPath = Paths.get(System.getProperty("user.dir"));
    }

    @Command
    public String showScripts() {
        String[] s = scriptManager.getScriptNames();
        return String.join("\n", s);
    }

    @Command
    public String pwd() {
        return currentPath.toString();
    }

    @Command
    public String ls() {
        String out = null;
        try {
            out = String.join("\n"
                    ,Files.walk(currentPath).map(Path::toString).collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
            out = e.getMessage();
        }
        return out;
    }

    @Command
    public String cd(String where) {
        Path p = Paths.get(currentPath.toString() + File.separator + where).normalize();
        if (!p.equals(currentPath)) {
            if (p.toFile().isFile()) return where + "isn't directory.";
            else if (!p.toFile().exists()) return where + " doesn't exists";
            else currentPath = p;
        }
        return "";
    }

    @Command
    public String run(String in) {
        StringBuilder out = new StringBuilder();
        File script = Paths.get(in)
                .normalize().toFile();
        if (script.isFile()) {
            try {
                scriptEngine.eval(new FileReader(script));
            } catch (ScriptException | FileNotFoundException e) {
                out.append(e.getMessage());
            }
        } else {
            script = Paths.get(currentPath.toString() + File.separator + in).normalize().toFile();
            if (script.isFile()) {
                try {
                    scriptEngine.eval(new FileReader(script));
                } catch (ScriptException | FileNotFoundException e) {
                    out.append(e.getMessage());
                }
            } else {
                out.append(in).append(" isn't a file.");
            }
        }
        return out.toString();
    }

    public static void main(String[] args) throws IOException {
        ShellFactory.createConsoleShell("command", "Welcome to Eikescrapper"
                , new ScriptRunnerApplication()).commandLoop();
    }
}
