package scripting;

import lombok.extern.apachecommons.CommonsLog;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@CommonsLog
final class ScriptManager {

    private Map<String, Script> scripts;
    private Map<Script, Thread> threadMap;
    private ScriptEngine scriptEngine;

    ScriptManager(ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
        scripts = new TreeMap<>();
        threadMap = new HashMap<>();
    }

    ScriptManager() {
        scriptEngine = new ScriptEngineManager().getEngineByName("jruby");
        scripts = new TreeMap<>();
        threadMap = new HashMap<>();
    }

    private static boolean preferredFile(File file) {
        return file.isFile();
    }

    void loadFromFolder(String path) {
        int before = 0;
        try (Stream<Path> walk = Files.walk(Paths.get(path)) ) {
            List<File> result = walk.filter(ScriptManager::preferredFile).map(Path::toFile).collect(Collectors.toList());
            for (File file : result) {
                scripts.putIfAbsent(file.getName(), new Script(file, scriptEngine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int added = scripts.size() - before;
        if (added > 0)
            log.info("Added " + added + " script" + ( (added > 1) ? "s" : "") + ".");
    }

    public void loadFromFile(File file) throws FileNotFoundException {
        if (preferredFile(file))
            scripts.putIfAbsent(file.getName(), new Script(file, scriptEngine));
    }

    public String[] getScriptNames() {
        String[] names = new String[scripts.size()];
        return this.scripts.keySet().toArray(names);
    }

    void startAll() {
        scripts.values().forEach(this::startScript);
    }

    public void startScript(String scriptName) {
        startScript(scripts.get(scriptName));
    }

    private void startScript(Script script) {
        if (script == null || (threadMap.containsKey(script) && script.isRunning())) return;
        Thread t = new Thread(script);
        threadMap.put(script, t);
        t.start();
    }

    private static boolean preferredFile(Path path) {
        return preferredFile(path.toFile());
    }
}
