package scripting;

import lombok.extern.java.Log;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;

@Log
class Script implements Runnable {
    private FileReader associatedFile;
    private String scriptName;
    private ScriptEngine scriptEngine;
    private boolean running;
    private int sleepingTime = 5000;
    private final static int runs = 3;

    public Script(File file, ScriptEngine engine) throws FileNotFoundException {
        associatedFile = new FileReader(file);
        scriptName = file.getName();
        scriptEngine = engine;
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setSleepingTime(int sleepingTime) {
        if (sleepingTime <= 0) throw new IllegalArgumentException("sleepingTime must be greater than zero");
        this.sleepingTime = sleepingTime;
    }

    @Override
    public void run() {
        int lruns = 0;
        running = true;
        while (running && (lruns < runs)) {
            try {
                scriptEngine.eval(this.associatedFile);
            } catch (ScriptException e) {
                running = false;
                log.warning("Couldn't run " + scriptName + ". Reason: " + e.getMessage());
            }
            try {
                Thread.sleep(sleepingTime);
            } catch (InterruptedException e) {
                running = false;
                log.info("Script: " + scriptName + " is stopping." );
            }
            lruns ++;
        }
        running = false;
    }

    public void stop() {
        running = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Script script = (Script) o;
        return sleepingTime == script.sleepingTime &&
                associatedFile.equals(script.associatedFile) &&
                scriptName.equals(script.scriptName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(associatedFile, scriptName, sleepingTime);
    }
}
