import lombok.extern.java.Log;

@Log
public class QueryManager implements Runnable {

//    private static Set<String> queries = new HashSet<String>() {
//        {
//            add("?q=hoje");
//            add("imoveis/aluguel?pe=400");
//        }
//    };
    private int refreshTimer = 5000;
    private String USERAGENT = "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/71.0.3578.98 Safari/537.36";

    public QueryManager() {
    }

    private void doWork() throws InterruptedException {
        String string = "https://al.olx.com.br/autos-e-pecas/carros-vans-e-utilitarios?o=";
        int limit = 100;
//        for (int i = 12; i < limit; i++) {
//            QueryWorker queryWorker = new QueryWorker(string + i);
//            queryWorker.work();
//            if (i%5 == 0) {
//                Thread.sleep(1000);
//            }
//        }
        throw new InterruptedException("Work ended.");
    }

    @Override
    public void run() {
        try {
            doWork();
        } catch (InterruptedException e) {
            log.warning (e.getMessage());
        }
    }
}
