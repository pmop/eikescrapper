package server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.IOException;


public class ControlServer {
    public class SomeResponse {
        public String text;
    }

    public class SomeRequest {
        public String text;
    }

    public Listener receiveListener = new Listener() {
        public void received (Connection connection, Object object) {
            if (object instanceof SomeRequest) {
                SomeRequest request = (SomeRequest) object;
                System.out.println(request.text);

                SomeResponse response = new SomeResponse();
                response.text = "Thankx";
                connection.sendTCP(response);
            }
        }
    };

    private void initServer() throws IOException {
        Server server = new Server();
        server.start();
        server.bind(54555,54777);
        server.addListener(receiveListener);
    }
}
