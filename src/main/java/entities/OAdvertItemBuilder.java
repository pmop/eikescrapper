package entities;

import org.apache.commons.pool2.impl.SoftReferenceObjectPool;
import utils.DOCDefs;

public class OAdvertItemBuilder {


    long   id;
    String link;
    String title;
    String city;
    String state;
    String neighbour;
    String publishedDate;
    String imgLink;
    String category;
    float  price;
    int    apiVerMajor;
    int    apiVerMinor;
    public final SoftReferenceObjectPool<OAdvertItem> oAdvertItemPoolRef;

    public OAdvertItemBuilder(SoftReferenceObjectPool<OAdvertItem> oAdvertItemPoolRef) {
        this.oAdvertItemPoolRef = oAdvertItemPoolRef;
        setApiVerMajor(DOCDefs.OLX_APIVERMAJOR);
        setApiVerMinor(DOCDefs.OLX_APIVERMINOR);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setNeighbour(String neighbour) {
        this.neighbour = neighbour;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setApiVerMajor(int apiVerMajor) {
        this.apiVerMajor = apiVerMajor;
    }

    public void setApiVerMinor(int apiVerMinor) {
        this.apiVerMinor = apiVerMinor;
    }

    public OAdvertItem getOdvertItem() throws Exception {
        OAdvertItem o = oAdvertItemPoolRef.borrowObject();
        o.setId(id);
        o.setLink(link);
        o.setTitle(title);
        o.setCity(city);
        o.setState(state);
        o.setNeighbour(neighbour);
        o.setPublishedDate(publishedDate);
        o.setImgLink(imgLink);
        o.setPrice(price);
        o.setCategory(category);
        o.setApiVerMajor(DOCDefs.OLX_APIVERMAJOR);
        o.setApiVerMinor(DOCDefs.OLX_APIVERMINOR);
        return o;
    }
}
