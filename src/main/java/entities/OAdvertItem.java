package entities;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


@Entity
@Table(name = "olx_items")
@Data
public class OAdvertItem {
    @Id
    @Column(name = "ID")
    long id;
    @Column(name = "LINK")
    String link;
    @Column(name = "TITLE")
    String title;
    @Column(name = "CITY")
    String city;
    @Column(name = "STATE")
    String state;
    @Column(name = "NEIGH")
    String neighbour;
    @Column(name = "PDATE")
    String publishedDate;
    @Column(name = "IMG_LINK")
    String imgLink;
    @Column(name = "CATEGORY")
    String category;
    @Column(name = "PRICE")
    float  price;
    @Column(name = "APIMAJOR")
    int    apiVerMajor;
    @Column(name = "APIMINOR")
    int    apiVerMinor;

    public OAdvertItem() {
        this.clear();
    }
    public OAdvertItem(OAdvertItem from) {
        set(        from.id,
                    from.link,
                    from.title,
                    from.city,
                    from.state,
                    from.neighbour,
                    from.publishedDate,
                    from.imgLink,
                    from.price,
                    from.apiVerMajor,
                    from.apiVerMinor
        );
    }

    public void set(long id,
                    String link,
                    String title,
                    String city,
                    String state,
                    String neighbour,
                    String publishedDate,
                    String imgLink,
                    float price,
                    int apiVerMajor,
                    int apiVerMinor) {
        this.id = id;
        this.link = link;
        this.title = title;
        this.city = city;
        this.state = state;
        this.neighbour = neighbour;
        this.publishedDate = publishedDate;
        this.imgLink = imgLink;
        this.price = price;
        this.apiVerMajor = apiVerMajor;
        this.apiVerMinor = apiVerMinor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, link, title, city, state, neighbour,
                imgLink, price, publishedDate, apiVerMajor, apiVerMinor);
    }
    public void clear(){
        id = 0;
        link = null;
        title = null;
        city = null;
        state = null;
        neighbour = null;
        publishedDate = null;
        imgLink = null;
        price = (float) 0.0;
        apiVerMajor = 0;
        apiVerMinor = 0;
    }
}
