package entities.memutils;

import entities.OAdvertItem;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

public class OAdvertItemFactory extends BasePooledObjectFactory<OAdvertItem> {
    @Override
    public OAdvertItem create() {
        return new OAdvertItem();
    }

    @Override
    public PooledObject<OAdvertItem> wrap(OAdvertItem oAdvertItem) {
        return new DefaultPooledObject<>(oAdvertItem);
    }

    @Override
    public void passivateObject(PooledObject<OAdvertItem> pooledObject) {
        pooledObject.getObject().clear();
    }
}
