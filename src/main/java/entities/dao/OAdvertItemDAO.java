package entities.dao;

import entities.OAdvertItem;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.java.Log;

import javax.persistence.*;

@Log
public class OAdvertItemDAO {
    private static EntityManagerFactory entityManagerFactory;

    public static void init(@NonNull EntityManagerFactory emfRef) {
        entityManagerFactory = emfRef;
    }


    @Synchronized
    public static int insertBatch(OAdvertItem[] oAdvertItem) {
        int howMany = 0;
        if (entityManagerFactory != null) {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction tx = null;
            for (OAdvertItem item : oAdvertItem) {
                if(insert(item)) howMany++;
            }
            entityManager.close();
        }
        return howMany;
    }

    @Synchronized
    public static boolean insert(OAdvertItem oAdvertItem) {
        boolean success = false;
        if (entityManagerFactory != null) {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            EntityTransaction tx = null;
            try {
                tx = entityManager.getTransaction();
                tx.begin();
                entityManager.persist(oAdvertItem);
                success = true;
                tx.commit();
            } catch (RollbackException rollBackException) {
                log.warning(rollBackException.getMessage());
                success = false;
                if (tx != null && tx.isActive()) tx.rollback();
            } catch (Exception e) {
                success = false;
                log.severe("Error: " + e.getMessage());
                if (tx != null && tx.isActive()) tx.rollback();
            }
            entityManager.close();
        }
        return success;
    }

    @Synchronized
    public boolean contains(long id) {
        boolean contains = false;
        if (entityManagerFactory != null) {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            TypedQuery<Integer> query =
                    entityManager.createNamedQuery
                            (String.format("select count(*) from olx_items o where o.id = %li", id),
                                    Integer.class);
            contains = query.getSingleResult().equals(1);
        }
        return contains;
    }

    public static void close() {
        if (entityManagerFactory != null) entityManagerFactory.close();
    }
}
