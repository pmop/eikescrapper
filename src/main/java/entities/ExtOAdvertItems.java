package entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "olx_items_ext")
@Data
public class ExtOAdvertItems {
    @Id
    long id;
    String body;
}
