package utils;

import lombok.extern.java.Log;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Log
public class DOCDefs {
    public final static String S_OLX_ADID = "name";
    public final static String ATTR_OLX_ID = "id";
    public final static String ATTR_OLX_ADLINK = "href";
    public final static String ATTR_OLX_ADTITLE = "title";
    public final static String S_OLX_ADPRICE = ".OLXad-list-price";
    public final static String S_OLX_ADREGION = ".OLXad-list-line-2>.detail-region";
    public final static String S_OLX_ADPDATE = ".col-4";
    public final static String S_OLX_CATEGORY = "p.text.detail-category";
    public final static String S_OLX_ADDATE_CNAME = "col-4";
    public final static String S_OLX_AD_DEFAULTIMG = "https://static.bn-static.com/img-49158/desktop/transparent.png";
    public final static String S_OLX_AD_IMG = ".image";
    public final static String S_OLX_AD_CATEGORY = "p.text.detail-category";
    public final static String ATTR_OLX_AD_IMG = "src";
    public final static String S_OLX_AD_ITEM = ".list>.item>.OLXad-list-link";
    public final static String OLX_URL = "https://al.olx.com.br/";
    public final static String[] PTMONTHS = new String[]{"janeiro", "fevereiro", "março", "abril", "maio",
            "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"};
    public final static List<String> PTMONTHS_ARR = new ArrayList<>(Arrays.asList(PTMONTHS));

    public final static int OLX_APIVERMAJOR = 0;
    public final static int OLX_APIVERMINOR = 1;

    public final static Dictionary<String, Integer> PTMonthsMap = new Hashtable<String, Integer>(PTMONTHS.length) {
        {
            for (int i = 0; i < DOCDefs.PTMONTHS.length; i++) {
                put(DOCDefs.PTMONTHS[i], i);
            }
        }
    };


    public static String getOlxDateString(String[] olxAdDate) {
        ZoneId localZone = ZoneId.systemDefault();
        LocalDate localDate = LocalDate.now(localZone);
        LocalDateTime localDateTime = localDate.atStartOfDay();

        int date = -1;
        String day = null;
        int hour = 0,
                minute = 0;
        try {
            if (olxAdDate.length == 3) {
                date = Integer.parseInt(olxAdDate[0]);
                day = olxAdDate[1];
                String[] hoursAndMinutes = olxAdDate[2].trim().split(":");
                hour = Integer.parseInt(hoursAndMinutes[0]);
                minute = Integer.parseInt(hoursAndMinutes[1]);
                //Date is based on 'day'
            } else if (olxAdDate.length == 2) {
                day = olxAdDate[0];
                String[] hoursAndMinutes = olxAdDate[1].trim().split(":");
                hour = Integer.parseInt(hoursAndMinutes[0]);
                minute = Integer.parseInt(hoursAndMinutes[1]);
            }
            if (day != null) {
                localDateTime = localDateTime.withHour(hour);
                localDateTime = localDateTime.withMinute(minute);
                if (PTMONTHS_ARR.contains(day)) {
                    localDateTime = localDateTime.withMonth(PTMonthsMap.get(day));
                } else if (day.equalsIgnoreCase("ontem")) {
                    localDateTime = localDateTime.minusDays(1);
                }  //Do nothing. It is already set to today
                if (date > 0) {
                    localDateTime.withDayOfMonth(date);
                }
            } else {
                log.warning("Unable to parse date from string: " + Arrays.toString(olxAdDate));
            }
        } catch (NullPointerException e) {
            log.warning("Unable to parse date from string: "
                    + Arrays.toString(olxAdDate) + "\tReason:\n" + e.getMessage());
        }
        return localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }
}
