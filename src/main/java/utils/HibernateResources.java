package utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateResources {
    private static EntityManagerFactory emf;

    public static void init() {
        emf = Persistence.createEntityManagerFactory("olx_items");
    }

    public static EntityManagerFactory getEmf() {
        return emf;
    }
    public static void close() {
        if (emf != null)
            emf.close();
    }
}
