package utils;

import entities.OAdvertItem;
import entities.memutils.OAdvertItemFactory;
import org.apache.commons.pool2.impl.SoftReferenceObjectPool;

import java.nio.file.Path;
import java.nio.file.Paths;

public class GlobalDefinitions {
    private boolean torIsRunning;
    public static Path currentExecutionPath;
    public static Path scriptsFolderPath;
    public final static String scriptsFolderName = "scripts";
    private static SoftReferenceObjectPool<OAdvertItem> oAdvertItemPool = null;

    public GlobalDefinitions() {
        currentExecutionPath = Paths
                .get(System.getProperty("user.dir"))
                .resolve(scriptsFolderName);
        torIsRunning = false;
        oAdvertItemPool = new SoftReferenceObjectPool<>(new OAdvertItemFactory());
    }

    public boolean startTorService(){
        return false;
    }

    public static SoftReferenceObjectPool<OAdvertItem> getOAdvertItemPool() {
        return oAdvertItemPool;
    }
}
